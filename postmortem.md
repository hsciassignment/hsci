> Did it go smoothly?

Pretty much yes. The tasks were simple and straight forward enough.

> Any surprises or lessons learned?

It's my first encounter with PHP's namespaces and I still can't say I've got the gist of it yet.

> Should we implement a production-ready version of what you build, or would you do things differently next time?

Well, this will work and everyone who clones the repo will get a working app. I however wouldn't release this
for developers to use, if it was up to me. 

To release it into the wild, I would much prefer to use the community provided cookbooks to manage software and manage those with librarian or berkshelf. 
Keeping a static copy of the `apt` cookbook feels wrong. 
I would also replace the `chef` directory with a git submodule and keep all cookbooks in a separate repo. Or even better, wrap the whole thing in a bash script, so installing librarian, pulling all cookbooks and/or
external repos and provisioning the box would happen with one command. Depending on the frequency of up/destroys per day, I also may pre-bake the box iamges with packer to cut on the download time.
