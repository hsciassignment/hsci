[![Build Status](https://travis-ci.org/hsciassignment/hsci.svg)](https://travis-ci.org/hsciassignment/hsci)


hello13
======

hello13 is a php application used for processing the string "Hello World, Composer!"
with a ROT13 function, and displaying the result on a web page, with a Shiba
Inu.

Dependencies for this application are managed with Composer (https://getcomposer.org/).
Tests utilize phpunit (https://phpunit.de/).

## TODO List:
* Create a new home on GitHub
* Set up TravisCI Integration
* Finish the Vagrantfile


## Usage:

Since this utilizes a git sumodule to pull the [apt cookbook](https://github.com/opscode-cookbooks/apt) needed by hello13, you need to clone the repo with `--recurse-submodules`. For a one-stop-shop, a convenient copy-paste:

`git clone https://github.com/hsciassignment/hsci.git --recurse-submodules; cd hsci && vagrant up`

