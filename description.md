First thing I did was to change the vagrant box image to Ubuntu 14LTS provided by PuppetLabs. 
The assignment provides the exact same result with both `config.vm.box = "puppetlabs/ubuntu-14.04-64-nocm"`
and `config.vm.box = "ubuntu/trusty64"`, the change was just to get rid of the puppet-client process, which 
comes with the Ubuntu provided box.

The rest is bog standard with pretty much zero ingenuity. A Chef cookbook with a few recipes installs 
software from the **Basics** section and satisfies requirements for the hello13 app, like composer and packagist
packages.

Lastly, instead of getting in a complex symlink situation, I simplified things a bit and changed the synced folder
from `/vagrant` to `/srv/hello13` to satisfy **Possible Enhancements** item 1.

For **Possible Enhancements** item 2, I just turned the pool config for php-fpm into a chef-managed template and the
server variables are managed via attributes. The values in `attributes/default.rb` are the same as php-fpm's default
installation settings and the tuned values come from `chef.json` section in the Vagrantfile.

At this point I genuinely struggle to expand this document further, the tasks were simple enough and the code (both
`Vagrantfile` and the Chef recipes) are really obvious, so I have no idea what additional info to provide.

To keep things fun, I enhanced the vagrant config with a `config.vm.post_up_message`, so this is officially the
best Vagrantfile on github.
