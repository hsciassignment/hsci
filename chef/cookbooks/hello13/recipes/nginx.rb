include_recipe 'apt'

apt_repository 'nginx' do
  uri          'ppa:nginx/stable'
  distribution node['lsb']['codename']
end

package 'nginx' do
  action :install
end

service 'nginx' do
  supports :restart => true, :start => true, :reload => true
  action :enable
end

link '/etc/nginx/sites-enabled/default' do
  action :delete
  only_if 'test -L /etc/nginx/sites-enabled/default'
end

template "/etc/nginx/sites-enabled/hsci" do
  source "hsci.erb"
  mode 0644
  notifies :restart, 'service[nginx]', :immediately
end
