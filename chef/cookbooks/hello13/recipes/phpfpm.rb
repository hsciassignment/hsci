package 'php5-fpm' do
  action :install
end

package 'php5-cli' do
  action :install
end

service 'php5-fpm' do
  supports :restart => true, :start => true, :reload => true
  action :enable
end

template "/etc/php5/fpm/pool.d/www.conf" do
  source "fpm-www.erb"
  mode 0644
  notifies :restart, 'service[php5-fpm]', :immediately
end
