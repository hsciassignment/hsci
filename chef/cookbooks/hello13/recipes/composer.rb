script "install_composer" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
  curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin
  EOH
end

execute "composer install" do
  command "/usr/local/bin/composer.phar install"
  cwd "/srv/hello13"
  action :run
end
