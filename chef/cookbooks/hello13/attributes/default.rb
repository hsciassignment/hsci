# The default values for php-fpm are taken from /etc/php5/fpm/pool.d/www.conf on a fresh Ubuntu 14LTS installation from package
default['hello13']['phpfpm']['poolname'] = "www"
default['hello13']['phpfpm']['pm.max_children'] = "5"
default['hello13']['phpfpm']['pm.start_servers'] = "2"
default['hello13']['phpfpm']['pm.min_spare_servers'] = "1"
default['hello13']['phpfpm']['pm.max_spare_servers'] = "3"
